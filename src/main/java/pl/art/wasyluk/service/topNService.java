package pl.art.wasyluk.service;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class topNService {

    public static List<Integer> topNStreams(List<Integer> list, int n) {

        if(list == null) throw new NullPointerException();
        if(n > list.size()) throw new RuntimeException("n must be lower then size of list!");

        return list.stream().sorted(Comparator.reverseOrder()).limit(n).collect(Collectors.toList());

    }

    public static List<Integer> topNBruteOnArray(List<Integer> list, int n) {

        if(list == null) throw new NullPointerException();
        if(n > list.size()) throw new RuntimeException("n must be lower then size of list!");

        Integer array[] = (Integer[]) list.toArray(new Integer[]{});
        List<Integer> topNedList = new ArrayList<>();
        Integer max;
        int maxIndex;

        for(int count = 0; count < n; count++) {
            max = Integer.MIN_VALUE;
            maxIndex = 0;
            for(int i = 0; i < array.length; i++) {
                if(array[i] > max) {
                    max = array[i];
                    maxIndex = i;
                }
            }
            topNedList.add(max);
            array[maxIndex] = Integer.MIN_VALUE;
        }
        return topNedList;
    }

    public static List<Integer> topNByTreeSet(List<Integer> list, int n) {


        if(list == null) throw new NullPointerException();
        if(n > list.size()) throw new RuntimeException("n must be lower then size of list!");

        List<Integer> topNedList = new ArrayList<>();

        TreeSet<List<Integer>> sortedLists = new TreeSet<>((l2, l1) -> l1.get(l1.size()-1).compareTo(l2.get(l2.size()-1)));
        int localMaxIndex = 0;
        int localMinIndex = 0;
        Integer maxValue;

        // Find sorted sublists

//        time = System.nanoTime();

        while(localMaxIndex < list.size()) {
            localMaxIndex = findLocalMax(localMinIndex, list);
            sortedLists.add(new ArrayList<>(list.subList(localMinIndex, localMaxIndex)));
            localMinIndex = localMaxIndex;

        }
//        System.out.print(" B:" + (System.nanoTime() - time) / 1000_000);

        // Find n greatests Integers

        for(int i = 0; i < n; i++) {
            List<Integer> listContainingMax = sortedLists.pollFirst();
            topNedList.add(listContainingMax.get(listContainingMax.size()-1));
            listContainingMax.remove(listContainingMax.size()-1);
            if(!listContainingMax.isEmpty()) {
                sortedLists.add(listContainingMax);
            }
        }
        return topNedList;
    }

    public static List<Integer> topNOptimized(List<Integer> list, int n) {


        if(list == null) throw new NullPointerException();
        if(n > list.size()) throw new RuntimeException("n must be lower then size of list!");

        List<Integer> topNedList = new ArrayList<>();

//        TreeSet<List<Integer>> sortedLists = new TreeSet<>((l2, l1) -> l1.get(l1.size()-1).compareTo(l2.get(l2.size()-1)));
        List<List<Integer>> sortedLists = new ArrayList<>();
        int localMaxIndex = 0;
        int localMinIndex = 0;
        Integer maxValue;

        // Find sorted sublists

//        long time = System.nanoTime();

        while(localMaxIndex < list.size()) {
            localMaxIndex = findLocalMax(localMinIndex, list);
            sortedLists.add(new ArrayList<>(list.subList(localMinIndex, localMaxIndex)));
            localMinIndex = localMaxIndex;
        }

//        System.out.print(" B: " + (System.nanoTime() - time) / 1000_000 + " B:");

        // Find n greatests Integers

        for(int i = 0; i < n; i++) {
            List<Integer> listContainingMax = findListContainingMax(sortedLists);
            topNedList.add(listContainingMax.get(listContainingMax.size()-1));
            listContainingMax.remove(listContainingMax.size()-1);
            if(listContainingMax.isEmpty()) {
                sortedLists.remove(listContainingMax);
            }
        }
        return topNedList;
    }



    private static int findLocalMax(int minIndex, List<Integer> list) {
        int maxIndex = minIndex+1;
        Integer maxValue = list.get(minIndex);
        while ((maxIndex < list.size()-1) && (list.get(maxIndex) >= maxValue)) {
            maxValue = list.get(maxIndex);
            maxIndex++;
        }
        return maxIndex;
    }

    private static List<Integer> findListContainingMax(List<List<Integer>> lists) {
        Integer max = Integer.MIN_VALUE;
        List<Integer> maxList = null;
        for(List<Integer> list: lists) {
            Integer localMax = list.get(list.size()-1);
            if(localMax.compareTo(max) > 0) {
                max = localMax;
                maxList = list;
            }
        }
        return maxList;
    }

}
