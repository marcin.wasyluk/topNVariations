package pl.art.wasyluk;

import pl.art.wasyluk.service.topNService;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class topNVariations {


    public static void main(String[] argv) {

        final int testSetSize = 10000;
        final int repeats = 1000;
        final int warmup = 2000;
        int probeSize = 50;
        boolean bruteOn = true;
        long lastStreamTime = 0;

        List<Integer> list = new ArrayList<>();
        Random random = new Random();
        long time;

        // Generate test list
        for(int i = 0; i < testSetSize; i++) {
            list.add(random.nextInt());
        }

        // Tests
        System.out.println("ProbeSize\tbyStreams\tbruteSort\toptimezedSort");

        for(probeSize = 10; probeSize <= testSetSize; probeSize+=50) {
            System.out.print(probeSize + "\t\t\t");
            for (int i = 0; i < warmup; i++) {
                topNService.topNStreams(list, probeSize);
            }

            time = System.nanoTime();
            for (int i = 0; i < repeats; i++) {
                topNService.topNStreams(list, probeSize);
            }
            lastStreamTime = System.nanoTime() - time;
            System.out.print((lastStreamTime / 1000_000) + "\t\t");


            if(bruteOn) {
                for (int i = 0; i < warmup; i++) {
                    topNService.topNBruteOnArray(list, probeSize);
                }
                time = System.nanoTime();
                for (int i = 0; i < repeats; i++) {
                    topNService.topNBruteOnArray(list, probeSize);
                }
                long bruteTime = System.nanoTime() - time;
                System.out.print(bruteTime/ 1000_000 + "\t\t");
                if(bruteTime > lastStreamTime) bruteOn = false;
            } else {
                System.out.print("*\t\t\t");
            }


            for (int i = 0; i < warmup; i++) {
                topNService.topNByTreeSet(list, probeSize);
            }
            time = System.nanoTime();
            for (int i = 0; i < repeats; i++) {
                topNService.topNByTreeSet(list, probeSize);
            }
            System.out.println("" + (System.nanoTime() - time) / 1000_000);
        }


    }


}
