import org.junit.Assert;
import org.junit.Test;
import pl.art.wasyluk.service.topNService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import static org.junit.Assert.assertEquals;

public class topNVariationsTests {

    @Test
    public void topNStreamTest() {
        List<Integer> list = Arrays.asList(3, 7, 2, 5, 6, 1);
        List<Integer> nList = topNService.topNStreams(list, 4);
        assertEquals(Arrays.asList(7, 6, 5, 3), nList);
    }

    @Test
    public void topNBruteOnArrayTest() {
        List<Integer> list = Arrays.asList(3, 7, 2, 5, 6, 1);
        List<Integer> nList = topNService.topNBruteOnArray(list, 4);
        assertEquals(Arrays.asList(7, 6, 5, 3), nList);
    }

    @Test
    public void topNbyTreeSetTest() {
        List<Integer> list = Arrays.asList(3, 7, 2, 5, 6, 6, 1);
        List<Integer> nList = topNService.topNByTreeSet(list, 5);
        assertEquals(Arrays.asList(7, 6, 6, 5, 3), nList);
    }

    @Test
    public void topNOptimizedTest() {
        List<Integer> list = Arrays.asList(3, 7, 2, 5, 6, 6, 1);
        List<Integer> nList = topNService.topNOptimized(list, 5);
        assertEquals(Arrays.asList(7, 6, 6, 5, 3), nList);
    }

    @Test
    public void topNOptimizedRandomTest() {
        final int testSetSize = 100000;
        int probeSize = 500;
        List<Integer> list = new ArrayList<>();
        Random random = new Random();

        for(int i = 0; i < testSetSize; i++) {
            list.add(random.nextInt());
        }
        List<Integer> nList1 = topNService.topNOptimized(list, probeSize);
        List<Integer> nList2 = topNService.topNStreams(list, probeSize);
        assertEquals(nList1, nList2);
    }

}
